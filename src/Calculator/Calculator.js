export default class Calculator {
  constructor(comporator) {
    this.comporator = comporator;
  }

  execute = ({inputText, patternText}) => this.comporator.execute(inputText, patternText)
}
