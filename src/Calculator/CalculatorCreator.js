import Calculator from './Calculator';
import Options from './Options';
import EntranceComparator from '../Comparator/EntranceComparator';
import EqualComparator from '../Comparator/EqualComparator';
import LevenshteinComparator from '../Comparator/LevenshteinComparator';

export default {
  create: (option) => {
    switch (option) {
      case Options.EQUAL:
        return new Calculator(new EqualComparator());
      case Options.ENTRANCE:
        return new Calculator(new EntranceComparator());
      case Options.LEVENSHTEIN:
        return new Calculator(new LevenshteinComparator());
      default:
        throw new TypeError(`Option ${option} doesn't support.`);
    }
  },
};

