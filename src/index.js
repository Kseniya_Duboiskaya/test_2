import Promise from 'bluebird';
import Reader from './Reader';
import Context from './Context';
import CalculatorCreator from './Calculator/CalculatorCreator';

const prompt = Promise.promisifyAll(require('prompt'));

const options = [{
  name:        'option',
  description: 'Choose mode one of the mode:  \r\n' +
               '1: Output all the lines from input.txt that match exactly any pattern in patterns.txt \r\n' +
               '2: Output all the lines from input.txt that contain a match from patterns.txt somewhere in the line. \r\n' +
               '3: Output all the lines from input.txt that contain a match with edit distance <= 1 patterns.txt',
  pattern:     /^[1-3]$/,
}];

function start() {
  prompt.start();

  prompt
    .getAsync(options)
    .get('option')
    .then(Number)
    .then((option) => {
      const inputPath   = './text/input.txt';
      const patternPath = './text/pattern.txt';
      const context     = new Context({inputPath, patternPath});
      const reader      = Promise.promisifyAll(new Reader(context));
      const calculator  = CalculatorCreator.create(option);
      return Promise
        .props({
          inputText:   reader.getInputTextAsync(),
          patternText: reader.getPatternTextAsync(),
        })
        .then(calculator.execute);
    })
    .then(console.log)
    .catch(console.log);
}

start();




