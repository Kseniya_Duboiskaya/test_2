export default class Context {
  constructor({inputPath, patternPath}) {
    this.inputPath   = inputPath;
    this.patternPath = patternPath;
  }
}
