import fs from 'fs';

export default class Reader {
  constructor(context) {
    this.context = context;
  }

  getInputText = cb => fs.readFile(this.context.inputPath, {encoding: 'UTF8'}, (err, result) => {
    if (err) {
      cb(err);
    }
    return cb(null, result.split('\r\n'));
  });

  getPatternText = cb => fs.readFile(this.context.patternPath, {encoding: 'UTF8'}, (err, result) => {
    if (err) {
      cb(err);
    }
    return cb(null, result.split('\r\n'));
  });
}
