export default class Composition {
  execute = () => {
    throw new TypeError('Composition is only interface');
  }
}
