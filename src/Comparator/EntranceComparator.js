import Comparator from './Comparator';

export default class EntranceComparator extends Comparator {
  execute = (textInput, textPattern) => textInput
    .reduce((arr, inputString) => {
      textPattern.forEach((patternString) => {
        if (inputString.indexOf(patternString) !== -1) {
          arr.push(inputString);
        }
      });
      return arr;
    }, [])
    .join('\r\n')
}
