import Comparator from './Comparator';

export default class EqualComparator extends Comparator {
  execute = (textInput, textPattern) => textInput
    .filter(inputString => textPattern.includes(inputString))
    .join('\r\n')
}
