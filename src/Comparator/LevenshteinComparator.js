import levenshtein from 'fast-levenshtein';
import Comparator from './Comparator';

export default class LevenshteinComparator extends Comparator {
  execute = (textInput, textPattern) => textInput
    .reduce((arr, inputString) => {
      textPattern.forEach((patternString) => {
        if (levenshtein.get(inputString, patternString) <= 1) {
          arr.push(inputString);
        }
      });
      return arr;
    }, [])
    .join('\r\n')
}
