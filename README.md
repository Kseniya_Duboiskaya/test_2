# Task 2

Write a Java application (command-line) that does the following:
Given two text files: input.txt and patterns.txt, where input.txt is a free-text document composed of 1 or
more lines of text, and patterns.txt is a set of search strings (1 per line). Your application should be able
to run in one of three different modes:
Required:
Output all the lines from input.txt that match exactly any pattern in patterns.txt
Optional:
Output all the lines from input.txt that contain a match from patterns.txt somewhere in the line.
Output all the lines from input.txt that contain a match with edit distance <= 1 patterns.txt
For example:
input.txt
Hello. This is line 1 of text.
and this is another.
line 3 here
the end

patterns.txt
the end
matches
line 3
and this is anoother.

Mode 1 outputs:
the end

Mode 2 outputs:
line 3 here
the end

Mode 3 outputs:
and this is another.
the end

# Instructions

## Settings:
1. Download and unzip the project.
2. Install node.
3. Go through the console to the root of the project. Run the command npm i.

## For start
1. Run the command npm start.
2. Select a mode.

# Motivation

I've used a beautiful command-line prompt for node.js. For reading text files I've used fs(The Node.js file system module allow you to work with the file).
For output all the lines from input.txt that contain a match with edit distance <= 1 patterns.txt used package fast-levenshtein.